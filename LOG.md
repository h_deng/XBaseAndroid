### 20171130（版本1.0.2）【已发布】
+ 解决了EventBus自动注册问题，取消自动注册
+ 解决了LoadingDialogBUG，已经修复。
+ 解决了判断内存卡是否存在的BUG。


### 20171204（版本1.0.3）【未发布】
+ 优化Activity启动，改为从ActivityUtils启动。
+ 新增复制黏贴工具类ClipboardUtils
+ 规范名称将Xbase改为XBase
+ 将倒计时控件 改为单列模式，规范使用模式。